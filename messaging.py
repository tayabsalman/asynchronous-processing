#!/usr/bin/python3
from celery import Celery
from celery.utils.log import get_task_logger
app=Celery('tasks',broker='amqp://')
logger=get_task_logger("__name__")
@app.task
def add(x,y):
    logger.info("Add 1")
    logger.info("Add 2")
    logger.info("Add 3")
    logger.info("Add 4")
    logger.info("Add function for x="+str(x)+" and y="+str(y))
    z=x+y
    logger.info("Add function yield = "+str(z))
    logger.info("Add 5")
    logger.info("Add 6")
    logger.info("Add 7")

@app.task
def sub(x,y):
    logger.info("Sub 1")
    logger.info("Sub 2")
    logger.info("Sub 3")
    logger.info("Sub 4")
    logger.info("Sub function for x="+str(x)+" and y="+str(y))
    z=x-y
    logger.info("Sub function yield = "+str(z))
    logger.info("Sub 5")
    logger.info("Sub 6")
    logger.info("Sub 7")


@app.task
def mul(x,y):
    logger.info("Mul 1")
    logger.info("Mul 2")
    logger.info("Mul 3")
    logger.info("Mul 4")
    logger.info("Mul function for x="+str(x)+" and y="+str(y))
    z=x*y
    logger.info("Mul function yield = "+str(z))
    logger.info("Mul 5")
    logger.info("Mul 6")
    logger.info("Mul 7")

@app.task
def div(x,y):
    logger.info("Div 1")
    logger.info("Div 2")
    logger.info("Div 3")
    logger.info("Div 4")
    logger.info("Division function for x="+str(x)+" and y="+str(y))
    z=x/y
    logger.info("Division function yield = "+str(z))
    logger.info("Div 5")
    logger.info("Div 6")
    logger.info("Div 7")

@app.task
def mod(x,y):
    logger.info("Mod 1")
    logger.info("Mod 2")
    logger.info("Mod 3")
    logger.info("Mod 4")
    logger.info("Mod function for x="+str(x)+" and y="+str(y))
    z=x%y
    logger.info("Mod function yield = "+str(z))
    logger.info("Mod 5")
    logger.info("Mod 6")
    logger.info("Mod 7")
